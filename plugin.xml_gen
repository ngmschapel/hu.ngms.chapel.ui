<?xml version="1.0" encoding="UTF-8"?>
<?eclipse version="3.0"?>

<plugin>

    <extension
            point="org.eclipse.ui.editors">
        <editor
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.editor.XtextEditor"
            contributorClass="org.eclipse.ui.editors.text.TextEditorActionContributor"
            default="true"
            extensions="chpl"
            id="hu.ngms.chapel.Chapel"
            name="Chapel Editor">
        </editor>
    </extension>
    <extension
        point="org.eclipse.ui.handlers">
        <handler
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.editor.hyperlinking.OpenDeclarationHandler"
            commandId="org.eclipse.xtext.ui.editor.hyperlinking.OpenDeclaration">
            <activeWhen>
                <reference
                    definitionId="hu.ngms.chapel.Chapel.Editor.opened">
                </reference>
            </activeWhen>
        </handler>
        <handler
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.editor.handler.ValidateActionHandler"
            commandId="hu.ngms.chapel.Chapel.validate">
         <activeWhen>
            <reference
                    definitionId="hu.ngms.chapel.Chapel.Editor.opened">
            </reference>
         </activeWhen>
      	</handler>
      	<!-- copy qualified name -->
        <handler
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedNameHandler"
            commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName">
            <activeWhen>
				<reference definitionId="hu.ngms.chapel.Chapel.Editor.opened" />
            </activeWhen>
        </handler>
        <handler
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedNameHandler"
            commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedName">
            <activeWhen>
            	<and>
            		<reference definitionId="hu.ngms.chapel.Chapel.XtextEditor.opened" />
	                <iterate>
						<adapt type="org.eclipse.xtext.ui.editor.outline.IOutlineNode" />
					</iterate>
				</and>
            </activeWhen>
        </handler>
    </extension>
    <extension point="org.eclipse.core.expressions.definitions">
        <definition id="hu.ngms.chapel.Chapel.Editor.opened">
            <and>
                <reference definitionId="isActiveEditorAnInstanceOfXtextEditor"/>
                <with variable="activeEditor">
                    <test property="org.eclipse.xtext.ui.editor.XtextEditor.languageName" 
                        value="hu.ngms.chapel.Chapel" 
                        forcePluginActivation="true"/>
                </with>        
            </and>
        </definition>
        <definition id="hu.ngms.chapel.Chapel.XtextEditor.opened">
            <and>
                <reference definitionId="isXtextEditorActive"/>
                <with variable="activeEditor">
                    <test property="org.eclipse.xtext.ui.editor.XtextEditor.languageName" 
                        value="hu.ngms.chapel.Chapel" 
                        forcePluginActivation="true"/>
                </with>        
            </and>
        </definition>
    </extension>
    <extension
            point="org.eclipse.ui.preferencePages">
        <page
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.editor.preferences.LanguageRootPreferencePage"
            id="hu.ngms.chapel.Chapel"
            name="Chapel">
            <keywordReference id="hu.ngms.chapel.ui.keyword_Chapel"/>
        </page>
        <page
            category="hu.ngms.chapel.Chapel"
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.editor.syntaxcoloring.SyntaxColoringPreferencePage"
            id="hu.ngms.chapel.Chapel.coloring"
            name="Syntax Coloring">
            <keywordReference id="hu.ngms.chapel.ui.keyword_Chapel"/>
        </page>
        <page
            category="hu.ngms.chapel.Chapel"
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.editor.templates.XtextTemplatePreferencePage"
            id="hu.ngms.chapel.Chapel.templates"
            name="Templates">
            <keywordReference id="hu.ngms.chapel.ui.keyword_Chapel"/>
        </page>
    </extension>
    <extension
            point="org.eclipse.ui.propertyPages">
        <page
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.editor.preferences.LanguageRootPreferencePage"
            id="hu.ngms.chapel.Chapel"
            name="Chapel">
            <keywordReference id="hu.ngms.chapel.ui.keyword_Chapel"/>
            <enabledWhen>
	            <adapt type="org.eclipse.core.resources.IProject"/>
			</enabledWhen>
	        <filter name="projectNature" value="org.eclipse.xtext.ui.shared.xtextNature"/>
        </page>
    </extension>
    <extension
        point="org.eclipse.ui.keywords">
        <keyword
            id="hu.ngms.chapel.ui.keyword_Chapel"
            label="Chapel"/>
    </extension>
    <extension
         point="org.eclipse.ui.commands">
      <command
            description="Trigger expensive validation"
            id="hu.ngms.chapel.Chapel.validate"
            name="Validate">
      </command>
      <!-- copy qualified name -->
      <command
            id="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName"
            categoryId="org.eclipse.ui.category.edit"
            description="Copy the qualified name for the selected element"
            name="Copy Qualified Name">
      </command>
      <command
            id="org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedName"
            categoryId="org.eclipse.ui.category.edit"
            description="Copy the qualified name for the selected element"
            name="Copy Qualified Name">
      </command>
    </extension>
    <extension point="org.eclipse.ui.menus">
        <menuContribution
            locationURI="popup:#TextEditorContext?after=group.edit">
             <command
                 commandId="hu.ngms.chapel.Chapel.validate"
                 style="push"
                 tooltip="Trigger expensive validation">
            <visibleWhen checkEnabled="false">
                <reference
                    definitionId="hu.ngms.chapel.Chapel.Editor.opened">
                </reference>
            </visibleWhen>
         </command>  
         </menuContribution>
         <!-- copy qualified name -->
         <menuContribution locationURI="popup:#TextEditorContext?after=copy">
         	<command commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName" 
         		style="push" tooltip="Copy Qualified Name">
            	<visibleWhen checkEnabled="false">
                	<reference definitionId="hu.ngms.chapel.Chapel.Editor.opened" />
            	</visibleWhen>
         	</command>  
         </menuContribution>
         <menuContribution locationURI="menu:edit?after=copy">
         	<command commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName"
            	style="push" tooltip="Copy Qualified Name">
            	<visibleWhen checkEnabled="false">
                	<reference definitionId="hu.ngms.chapel.Chapel.Editor.opened" />
            	</visibleWhen>
         	</command>  
         </menuContribution>
         <menuContribution locationURI="popup:org.eclipse.xtext.ui.outline?after=additions">
			<command commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedName" 
				style="push" tooltip="Copy Qualified Name">
         		<visibleWhen checkEnabled="false">
	            	<and>
	            		<reference definitionId="hu.ngms.chapel.Chapel.XtextEditor.opened" />
						<iterate>
							<adapt type="org.eclipse.xtext.ui.editor.outline.IOutlineNode" />
						</iterate>
					</and>
				</visibleWhen>
			</command>
         </menuContribution>
    </extension>
    <extension point="org.eclipse.ui.menus">
		<menuContribution locationURI="popup:#TextEditorContext?endof=group.find">
			<command commandId="org.eclipse.xtext.ui.editor.FindReferences">
				<visibleWhen checkEnabled="false">
                	<reference definitionId="hu.ngms.chapel.Chapel.Editor.opened">
                	</reference>
            	</visibleWhen>
			</command>
		</menuContribution>
	</extension>
	<extension point="org.eclipse.ui.handlers">
	    <handler
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.editor.findrefs.FindReferencesHandler"
            commandId="org.eclipse.xtext.ui.editor.FindReferences">
            <activeWhen>
                <reference
                    definitionId="hu.ngms.chapel.Chapel.Editor.opened">
                </reference>
            </activeWhen>
        </handler>
    </extension>   

<!-- adding resource factories -->

	<extension
		point="org.eclipse.emf.ecore.extension_parser">
		<parser
			class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.resource.IResourceFactory"
			type="chpl">
		</parser>
	</extension>
	<extension point="org.eclipse.xtext.extension_resourceServiceProvider">
        <resourceServiceProvider
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.resource.IResourceUIServiceProvider"
            uriExtension="chpl">
        </resourceServiceProvider>
    </extension>


	<!-- marker definitions for hu.ngms.chapel.Chapel -->
	<extension
	        id="chapel.check.fast"
	        name="Chapel Problem"
	        point="org.eclipse.core.resources.markers">
	    <super type="org.eclipse.xtext.ui.check.fast"/>
	    <persistent value="true"/>
	</extension>
	<extension
	        id="chapel.check.normal"
	        name="Chapel Problem"
	        point="org.eclipse.core.resources.markers">
	    <super type="org.eclipse.xtext.ui.check.normal"/>
	    <persistent value="true"/>
	</extension>
	<extension
	        id="chapel.check.expensive"
	        name="Chapel Problem"
	        point="org.eclipse.core.resources.markers">
	    <super type="org.eclipse.xtext.ui.check.expensive"/>
	    <persistent value="true"/>
	</extension>

   <extension
         point="org.eclipse.xtext.builder.participant">
      <participant
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.builder.IXtextBuilderParticipant"
            fileExtensions="chpl"
            >
      </participant>
   </extension>
   <extension
            point="org.eclipse.ui.preferencePages">
        <page
            category="hu.ngms.chapel.Chapel"
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.builder.preferences.BuilderPreferencePage"
            id="hu.ngms.chapel.Chapel.compiler.preferencePage"
            name="Compiler">
            <keywordReference id="hu.ngms.chapel.ui.keyword_Chapel"/>
        </page>
    </extension>
    <extension
            point="org.eclipse.ui.propertyPages">
        <page
            category="hu.ngms.chapel.Chapel"
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.builder.preferences.BuilderPreferencePage"
            id="hu.ngms.chapel.Chapel.compiler.propertyPage"
            name="Compiler">
            <keywordReference id="hu.ngms.chapel.ui.keyword_Chapel"/>
            <enabledWhen>
	            <adapt type="org.eclipse.core.resources.IProject"/>
			</enabledWhen>
	        <filter name="projectNature" value="org.eclipse.xtext.ui.shared.xtextNature"/>
        </page>
    </extension>
    <extension point="org.eclipse.ui.menus">
		<menuContribution locationURI="popup:#TextEditorContext?after=xtext.ui.openDeclaration">
			<command
				commandId="org.eclipse.xtext.ui.OpenGeneratedFileCommand"
				id="hu.ngms.chapel.Chapel.OpenGeneratedCode"
				style="push">
					<visibleWhen checkEnabled="false">
						<reference definitionId="hu.ngms.chapel.Chapel.Editor.opened" />
					</visibleWhen>
			</command>
		</menuContribution>
	</extension>
	<extension point="org.eclipse.ui.handlers">
		<handler
			class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.generator.trace.OpenGeneratedFileHandler"
			commandId="org.eclipse.xtext.ui.OpenGeneratedFileCommand">
				<activeWhen>
					<reference definitionId="hu.ngms.chapel.Chapel.Editor.opened" />
				</activeWhen>
		</handler>
	</extension>

	<!-- Quick Outline -->
	<extension
		point="org.eclipse.ui.handlers">
		<handler 
			class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.editor.outline.quickoutline.ShowQuickOutlineActionHandler"
			commandId="org.eclipse.xtext.ui.editor.outline.QuickOutline">
			<activeWhen>
				<reference
					definitionId="hu.ngms.chapel.Chapel.Editor.opened">
				</reference>
			</activeWhen>
		</handler>
	</extension>
	<extension
		point="org.eclipse.ui.commands">
		<command
			description="Open the quick outline."
			id="org.eclipse.xtext.ui.editor.outline.QuickOutline"
			name="Quick Outline">
		</command>
	</extension>
	<extension point="org.eclipse.ui.menus">
		<menuContribution
			locationURI="popup:#TextEditorContext?after=group.open">
			<command commandId="org.eclipse.xtext.ui.editor.outline.QuickOutline"
				style="push"
				tooltip="Open Quick Outline">
				<visibleWhen checkEnabled="false">
					<reference definitionId="hu.ngms.chapel.Chapel.Editor.opened"/>
				</visibleWhen>
			</command>
		</menuContribution>
	</extension>
    <!-- quickfix marker resolution generator for hu.ngms.chapel.Chapel -->
    <extension
            point="org.eclipse.ui.ide.markerResolution">
        <markerResolutionGenerator
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.editor.quickfix.MarkerResolutionGenerator"
            markerType="hu.ngms.chapel.ui.chapel.check.fast">
            <attribute
                name="FIXABLE_KEY"
                value="true">
            </attribute>
        </markerResolutionGenerator>
        <markerResolutionGenerator
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.editor.quickfix.MarkerResolutionGenerator"
            markerType="hu.ngms.chapel.ui.chapel.check.normal">
            <attribute
                name="FIXABLE_KEY"
                value="true">
            </attribute>
        </markerResolutionGenerator>
        <markerResolutionGenerator
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.editor.quickfix.MarkerResolutionGenerator"
            markerType="hu.ngms.chapel.ui.chapel.check.expensive">
            <attribute
                name="FIXABLE_KEY"
                value="true">
            </attribute>
        </markerResolutionGenerator>
    </extension>
   	<!-- Rename Refactoring -->
	<extension point="org.eclipse.ui.handlers">
		<handler 
			class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.refactoring.ui.DefaultRenameElementHandler"
			commandId="org.eclipse.xtext.ui.refactoring.RenameElement">
			<activeWhen>
				<reference
					definitionId="hu.ngms.chapel.Chapel.Editor.opened">
				</reference>
			</activeWhen>
		</handler>
	</extension>
    <extension point="org.eclipse.ui.menus">
         <menuContribution
            locationURI="popup:#TextEditorContext?after=group.edit">
         <command commandId="org.eclipse.xtext.ui.refactoring.RenameElement"
               style="push">
            <visibleWhen checkEnabled="false">
               <reference
                     definitionId="hu.ngms.chapel.Chapel.Editor.opened">
               </reference>
            </visibleWhen>
         </command>
      </menuContribution>
   </extension>
   <extension point="org.eclipse.ui.preferencePages">
	    <page
	        category="hu.ngms.chapel.Chapel"
	        class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.refactoring.ui.RefactoringPreferencePage"
	        id="hu.ngms.chapel.Chapel.refactoring"
	        name="Refactoring">
	        <keywordReference id="hu.ngms.chapel.ui.keyword_Chapel"/>
	    </page>
	</extension>

  <extension point="org.eclipse.compare.contentViewers">
    <viewer id="hu.ngms.chapel.Chapel.compare.contentViewers"
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.compare.InjectableViewerCreator"
            extensions="chpl">
    </viewer>
  </extension>
  <extension point="org.eclipse.compare.contentMergeViewers">
    <viewer id="hu.ngms.chapel.Chapel.compare.contentMergeViewers"
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.compare.InjectableViewerCreator"
            extensions="chpl" label="Chapel Compare">
     </viewer>
  </extension>
  <extension point="org.eclipse.ui.editors.documentProviders">
    <provider id="hu.ngms.chapel.Chapel.editors.documentProviders"
            class="hu.ngms.chapel.ui.ChapelExecutableExtensionFactory:org.eclipse.xtext.ui.editor.model.XtextDocumentProvider"
            extensions="chpl">
    </provider>
  </extension>
  <extension point="org.eclipse.team.core.fileTypes">
    <fileTypes
            extension="chpl"
            type="text">
    </fileTypes>
  </extension>

</plugin>
